# Unreales Game

## **Todo:**

### Grafik:
- [x] GUI
    - [x] Sauerstoffanzeige/Timer
    - [x] Menü
        - [x] Resume
        - [x] Exit
        - [x] Options
            - [x] Audio
                - [x] Lautstärke
                    - [x] Effekte
                    - [x] Musik
            - [x] Difficulty
- [x] Charaktere
    - [x] 3 Kinder auf dem Schulhof
    - [x] Schulrektor
    - [x] 1 NPC (Greifhakenspasti)
    - [x] Hauptcharakter
- [x] Hintergründe
    - [x] Schulhof
    - [x] Level 1
    - [x] Brennendes Haus
- [x] Tileset
    - [x] Feuer
- [x] Animationen
    - [x] 3 Kinder auf dem Schulhof
        - [x] Idle
    - [x] Schulrektor
        - [x] Idle
    - [x] 1 NPC (Greifhakenspasti)
        - [x] Idle
    - [x] Hauptcharakter
        - [x] Idle
        - [x] Laufen
        - [x] Springen/Landen
        - [x] Grapple werfen
        - [x] Grapple klettern
        - [x] Ducken
    - [x] evtl. Tileset-animationen


### Sound:
- [x] GUI Design
    - [x] Buttons
    - [x] Slider
    - [ ] ~~Checkbox~~
- [x] Hintergrundmusik
    - [x] Titlescreen
    - [x] Action?
    - [x] Ruhig?
- [ ] Soundeffekte:
    - [ ] Springen
    - [ ] Laufen
    - [ ] Grapple
    - [ ] Feuer
    - [ ] Sprechen
    - [ ] Tod
    - [ ] Interaktionen mit Tileset
        - [ ] Springen
        - [ ] Laufen
        - [ ] Sterben
        - [ ] Grapple?

### Code:
- [x] Hauptcharakter (BP)
    - [x] Steuerung
        - [x] Tastertur
            - [x] Grapple
            - [x] Links/rechts
            - [x] Ducken
            - [x] Springen
        - [x] Controller
            - [x] Grapple
            - [x] Links/rechts
            - [x] Ducken
            - [x] Springen
    - [x] Kamera (Folgt Horizontal dem Charakter, ist auf Hintergrundhöhe Begrenzt)
- [x] Checkpoints (BP)
- [x] Timer (CPP)
    - [x] An (läuft durch)
    - [x] Aus (leichte Schwierigkeit)
    - [x] wird bei Checkpoint gespeichert & bei Tod wiederhergestellt
- [x] Obstacles (CPP?)
    - [x] Feuer
    - [x] Box
    - [x] Platform
