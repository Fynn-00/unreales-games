// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PaperFlipbookComponent.h"
#include "Components/PointLightComponent.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Checkpoint.generated.h"

UCLASS()
class MYPROJECT3_API ACheckpoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckpoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UBoxComponent* CollisionBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UPaperFlipbookComponent* FlipBook;

	UMaterial* Material;
	UMaterialInstance* FireMaterial;
	UStaticMesh* FireMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* FakeLight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector MainCharLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USoundCue* DeathCue = LoadObject<USoundCue>(nullptr, TEXT("/Game/2DSideScroller/Sound/SFX/Death_Cue.Death_Cue"));

	UPROPERTY(VisibleAnywhere)
		UAudioComponent* DeathAudio;
	
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
