// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Timer.generated.h"

UCLASS()
class MYPROJECT3_API ATimer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATimer();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Timer Settings")
		float Timer = 60;

	// The Initial Startingpoint for the Timer for loading later
	UPROPERTY(BlueprintReadOnly, Category = "Timer Settings")
		float InitTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	void TimerFunction();

	FTimerHandle TimerHandle;
};
