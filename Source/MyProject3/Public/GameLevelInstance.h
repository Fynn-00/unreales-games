// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameLevelInstance.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API UGameLevelInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	// Timer value for loading in new Level
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float Timer;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float InitialTimer;

	// 0 = easy, 1 = hard
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool Diffculty = true;

	// Load the Level Music into a Variable
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		USoundCue* LevelMusic = LoadObject<USoundCue>(nullptr, TEXT("/Game/2DSideScroller/Sound/Music/FIRE____Cue.FIRE____Cue"));

	UPROPERTY(BlueprintReadOnly)
		UAudioComponent* Music;

	UFUNCTION(BlueprintCallable)
		void PlayLevelMusic() 
		{
			if (!Music)
			{
				Music = UGameplayStatics::CreateSound2D(GetWorld()->GetFirstPlayerController()->GetPawn(), LevelMusic, 1.f, 1.0f, 0.0f, nullptr, true, false);
			}
			Music->Play(0.f);
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, "Music Started");
		}
	UFUNCTION(BlueprintCallable)
		void StopLevelMusic()
		{
			if (!Music)
			{
				Music = UGameplayStatics::CreateSound2D(GetWorld()->GetFirstPlayerController()->GetPawn(), LevelMusic, 1.f, 1.0f, 0.0f, nullptr, true, false);
			}
			Music->Stop();
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, "Music Stopped");
		}
};