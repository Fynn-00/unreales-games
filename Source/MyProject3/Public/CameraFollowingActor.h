// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraFollowingActor.generated.h"

UCLASS()
class MYPROJECT3_API ACameraFollowingActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraFollowingActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetNewPos();

	// Sets the Actor that this Object should Follow
	AActor* FollowingActor;
	// Contains the Position of the Actor to Follow
	FVector FActorLoc;

	float NewX, NewZ;

	// Set Borders that the Camera shouldn't be able to look over
	// This is meant to be the Hitbox of the Level walls so that it can be used with offets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		AActor* LeftBorder;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		AActor* RightBorder;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		AActor* TopBorder;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		AActor* BottomBorder;

	// Offset from the Level Walls. This should be half of the Ortho Width(X)
	// For Y it can be calculated by: (OrthoWidth/AspectRatio)/2
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		float BorderOffsetX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Borders")
		float BorderOffsetZ;

	UPROPERTY(BlueprintReadOnly)
		float Max_X;
	UPROPERTY(BlueprintReadOnly)
		float Min_X;
	UPROPERTY(BlueprintReadOnly)
		float Max_Z;
	UPROPERTY(BlueprintReadOnly)
		float Min_Z;
};
