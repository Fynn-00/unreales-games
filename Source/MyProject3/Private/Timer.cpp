// Fill out your copyright notice in the Description page of Project Settings.

#include "Timer.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/GameInstance.h"
#include "GameLevelInstance.cpp"

// Sets default values
ATimer::ATimer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ATimer::BeginPlay()
{
	Super::BeginPlay();

	// Set the Timer to the Value 
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ATimer::TimerFunction, 1.0f, true, 1.0f);

	// Save the Initial Timer for Recovery on Level load
	InitTimer = Timer;

	// Recover Timer from GameInstance if needed
	if (((UGameLevelInstance*)GetWorld()->GetGameInstance())->Timer > 0.f) 
	{
		Timer = ((UGameLevelInstance*)GetWorld()->GetGameInstance())->Timer;
	}
}

// Counts down the Timer and Clears on reaching 0
void ATimer::TimerFunction()
{
	Timer--;
	if (Timer == 0)
	{
		GetWorldTimerManager().ClearTimer(TimerHandle);
	}
}
