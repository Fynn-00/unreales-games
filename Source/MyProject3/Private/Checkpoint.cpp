// Fill out your copyright notice in the Description page of Project Settings.


#include "Checkpoint.h"
#include "Components/BoxComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"

#define PrintString(String) GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, String);

// Sets default values
ACheckpoint::ACheckpoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Checkpoint"));
	CollisionBox->SetBoxExtent(FVector(32.f, 32.f, 32.f));
	CollisionBox->SetCollisionProfileName("Trigger");
	RootComponent = CollisionBox;

	FlipBook = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipBookComponent"));

	FakeLight = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LightComponent"));
	
	
	Material = LoadObject<UMaterial>(nullptr, TEXT("/Game/FakeLightMaterial2.FakeLightMaterial2"));
	FireMaterial = LoadObject<UMaterialInstance>(nullptr, TEXT("/Game/2DSideScroller/FakeLightMaterial_Inst.FakeLightMaterial_Inst"));
	FireMesh = LoadObject<UStaticMesh>(nullptr, TEXT("/Engine/BasicShapes/Plane.Plane"));

	FakeLight->SetStaticMesh(FireMesh);
	FakeLight->SetMaterial(0, FireMaterial);
	
	
	

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::OnOverlapBegin);
	CollisionBox->OnComponentEndOverlap.AddDynamic(this, &ACheckpoint::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ACheckpoint::BeginPlay()
{
	Super::BeginPlay();
	FakeLight->SetRelativeLocation(FVector(0,-10,0));
	FakeLight->SetRelativeRotation(FRotator(0,0,90));
	FakeLight->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	FakeLight->SetRelativeScale3D(FVector(5.f,5.f,5.f));
	DeathAudio = UGameplayStatics::CreateSound2D(GetWorld()->GetFirstPlayerController()->GetPawn(), DeathCue, 1.f, 1.0f, 0.3f, nullptr, true, false);
	UMaterialInstanceDynamic* FlipBookMaterial = FlipBook->CreateDynamicMaterialInstance(0, Material);
	FlipBookMaterial->SetVectorParameterValue(FName("LightColor"), FLinearColor(2.f, 2.f, 2.f));
	
}

// Called every frame
void ACheckpoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACheckpoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, "Overlap Begin Called");

	if (OtherActor == GetWorld()->GetFirstPlayerController()->GetPawn()){
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
		DeathAudio->Play();
	}
}

void ACheckpoint::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, "Overlap End Called");
}