// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraFollowingActor.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "Misc/App.h"

#define PrintString(String) GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, String);

// Sets default values
ACameraFollowingActor::ACameraFollowingActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACameraFollowingActor::BeginPlay()
{
	Super::BeginPlay();
	
	FollowingActor = GetWorld()->GetFirstPlayerController()->GetPawn();

	// Checks for all needed Actors and then sets the Right Border Locations
	if (IsValid(FollowingActor) && IsValid(LeftBorder) && IsValid(RightBorder))
	{
		Max_X = RightBorder->GetActorLocation().X - BorderOffsetX;
		Min_X = LeftBorder->GetActorLocation().X + BorderOffsetX;
		Min_Z = BottomBorder->GetActorLocation().Z + BorderOffsetZ;
		Max_Z = TopBorder->GetActorLocation().Z - BorderOffsetZ;
		SetNewPos();
	}
}

// Called every frame
void ACameraFollowingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetNewPos();
}

// Sets a new Position of Self within the given Borders
void ACameraFollowingActor::SetNewPos()
{
	FVector FActorLocInterp = FMath::VInterpTo(GetActorLocation(), FollowingActor->GetActorLocation(), FApp::GetDeltaTime(), 50.f);

	// Set own Location on X axis
	NewX = FMath::Clamp(FActorLocInterp.X, Min_X, Max_X);

	// Set own Location on Z axis
	NewZ = FMath::Clamp(FActorLocInterp.Z, Min_Z, Max_Z);

	SetActorLocation(FVector(NewX, GetActorLocation().Y, NewZ), false, 0, ETeleportType::None);
}

